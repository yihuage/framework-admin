const ExtractTextPlugin = require('extract-text-webpack-plugin')
const merge = require('webpack-merge')
const webpack = require('webpack')
const path = require('path')

module.exports = Object.assign({}, require('./webpack.config'), { // 沿用项目的各自配置
  output: {
    path: path.join(__dirname, '/dist'),
    filename: '[name].js',
    library: '[name]' // 当前Dll的所有内容都会存放在这个参数指定变量名的一个全局变量下，注意与DllPlugin的name参数保持一致
  },
  entry: {
    /*
     指定需要打包的js模块
     或是css/less/图片/字体文件等资源，但注意要在module参数配置好相应的loader
     */
    dll: [
      'jquery',
      'jstree',
      'toastr',
      'bootstrap',
      'sweetalert'
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      compress: {
        warnings: false
      },
      comments: false
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new webpack.DllPlugin({
      context: __dirname,
      path: path.join(__dirname, 'manifest.json'),
      name: '[name]'
    })
  ]
})